# Bug Report

Thanks for taking the time to fill out this bug report!

Before creating bug reports, please check before-submitting-a-bug-report, as you might find out that you don't need to create one. When you are creating a bug report, please include as many details as possible.

#### Before Submitting A Bug Report

* **Check the README.md.** You might be able to find the cause of the problem and fix things yourself. Most importantly, check if you can reproduce the problem __in the latest version__.
* **Work through the troubleshooting process.** Troubleshooting will include changing the log level of Itential Automation Platform (IAP) and adapters and checking the logs to see what the issues are. These logs should be included in any ticket you open for this issue.
* **Check for resolutions to the issue.** Check the <a href="https://itential.atlassian.net/servicedesk/customer/kb/view/286883841?applicationId=605994d2-2cb2-3174-af59-ed5b23ff5fd5&spaceKey=PKB&src=1187380921" target="_blank">Itential Knowledge Base</a> to see if there is a known resolution for the issue you are having.
* **Ask around in chat if you are an Itential employee** to see if others are experiencing the same issue.

#### How to Submit a (Good) Bug Report

Please read the guidlines and move to describe-the-bug. Explain the problem and include additional details to help maintainers reproduce the problem:

- **Use a clear and descriptive title** for the issue to identify the problem.
- **Describe the exact steps which reproduce the problem** in as much detail as possible. For example, start by explaining how you configured the adapter (e.g., which properties you used and how they were set) or how you are using an artifact.
- **Provide specific examples to demonstrate the steps**. Include logs, or copy/paste snippets, in your examples.
- **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
- **Explain which behavior you expected to see instead and why.**
- **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem. You can use <a href="https://www.cockos.com/licecap/" target="_blank">this tool</a> to record GIFs on macOS and Windows. Use <a href="https://github.com/colinkeenan/silentcast" target="_blank">this tool</a> or <a href="https://github.com/rhcarvalho/byzanz-guiz" target="_blank">program</a> on Linux.
- **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

- **Did the problem start happening recently** (e.g. after updating to a new version/tag) or was this always a problem?
- If the problem started happening recently, **can you reproduce the problem in an older version/tag?** What's the most recent version in which the problem does not happen?
- **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.

Include details about your configuration and environment:

- **Which version of the adapter/artifact are you using?** You can get the exact version by checking the project version in the package.json file.
- **What's the name and version of the OS you're using**?
- **Are you running or using IAP in a virtual machine?** If so, which VM software are you using and which operating systems and versions are used for the host and guest?
- **Are there firewalls between IAP and any other system you are integrating with?** If so, have you tried running curl from the system IAP is on to the other system to verify you have connectivity between the systems?

## Describe the bug

_[A clear and concise description of what the bug is.]_

## IAP Version

_[List the IAP Version that the bug is happening in.]_

## NPM Package version

_[List the npm package version.]_

## Steps To Reproduce

_[List out the exact steps to reproduce the issue here.]_

### Expected behavior

_[A clear and concise description of what you expected to happen.]_

### Current behavior

_[List out the current behavior here.]_

### Screenshots/Video

_[If applicable, add screenshots/video to help explain or demo the issue here.]_

## Possible fixes

_[If you can, link to the line of code that might be responsible for the problem.]_

## Additional context

_[Add any other context about the problem here.]_

/label ~"Issue Type::Bug"
/assign @ishita.prakash
