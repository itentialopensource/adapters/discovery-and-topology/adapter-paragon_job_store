## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Paragon Job Store. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Paragon Job Store.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Paragon_job_store. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceUpdateJob(iD, body, callback)</td>
    <td style="padding:15px">Update job by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceGetJob(iD, detail, fields, refFields, callback)</td>
    <td style="padding:15px">Get job by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceDeleteJob(iD, callback)</td>
    <td style="padding:15px">Delete job by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkListJob(body, callback)</td>
    <td style="padding:15px">Bulk list jobs</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-list-job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkExtRefUpdate(body, callback)</td>
    <td style="padding:15px">Bulk external reference updates</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-ext-ref-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkRefUpdate(body, callback)</td>
    <td style="padding:15px">Bulk reference updates</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-ref-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkListJobPurgePolicy(body, callback)</td>
    <td style="padding:15px">Bulk list job-purge-policys</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-list-job-purge-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceUpdateLastPublishedNotification(iD, body, callback)</td>
    <td style="padding:15px">Update last-published-notification by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/last-published-notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceGetLastPublishedNotification(iD, detail, fields, refFields, callback)</td>
    <td style="padding:15px">Get last-published-notification by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/last-published-notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceDeleteLastPublishedNotification(iD, callback)</td>
    <td style="padding:15px">Delete last-published-notification by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/last-published-notification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkListTask(body, callback)</td>
    <td style="padding:15px">Bulk list tasks</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-list-task?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceCreateLastPublishedNotification(body, callback)</td>
    <td style="padding:15px">Create last-published-notification</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/last-published-notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceListLastPublishedNotification(specSize, specPageMarker, specDetail, specCount, specExcludeShared, specExcludeHrefs, specParentFqNameStr, specParentType, specParentId, specBackRefId, specObjUuids, specFields, specFilters, specRefUuids, specFrom, specSortby, specOperation = 'AND', specTagFilters, specTagDetail, specRefFields, specExtRefUuids, callback)</td>
    <td style="padding:15px">List last-published-notifications</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/last-published-notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceCreateJobPurgePolicy(body, callback)</td>
    <td style="padding:15px">Create job-purge-policy</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job-purge-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceListJobPurgePolicy(specSize, specPageMarker, specDetail, specCount, specExcludeShared, specExcludeHrefs, specParentFqNameStr, specParentType, specParentId, specBackRefId, specObjUuids, specFields, specFilters, specRefUuids, specFrom, specSortby, specOperation = 'AND', specTagFilters, specTagDetail, specRefFields, specExtRefUuids, callback)</td>
    <td style="padding:15px">List job-purge-policys</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job-purge-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceCreateTask(body, callback)</td>
    <td style="padding:15px">Create task</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/task?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceListTask(specSize, specPageMarker, specDetail, specCount, specExcludeShared, specExcludeHrefs, specParentFqNameStr, specParentType, specParentId, specBackRefId, specObjUuids, specFields, specFilters, specRefUuids, specFrom, specSortby, specOperation = 'AND', specTagFilters, specTagDetail, specRefFields, specExtRefUuids, callback)</td>
    <td style="padding:15px">List tasks</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/task?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceRefUpdate(body, callback)</td>
    <td style="padding:15px">Reference update</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/ref-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceExtRefUpdate(body, callback)</td>
    <td style="padding:15px">External reference update</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/ext-ref-update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceCreateJob(body, callback)</td>
    <td style="padding:15px">Create job</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceListJob(specSize, specPageMarker, specDetail, specCount, specExcludeShared, specExcludeHrefs, specParentFqNameStr, specParentType, specParentId, specBackRefId, specObjUuids, specFields, specFilters, specRefUuids, specFrom, specSortby, specOperation = 'AND', specTagFilters, specTagDetail, specRefFields, specExtRefUuids, callback)</td>
    <td style="padding:15px">List jobs</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceUpdateJobPurgePolicy(iD, body, callback)</td>
    <td style="padding:15px">Update job-purge-policy by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job-purge-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceGetJobPurgePolicy(iD, detail, fields, refFields, callback)</td>
    <td style="padding:15px">Get job-purge-policy by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job-purge-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceDeleteJobPurgePolicy(iD, callback)</td>
    <td style="padding:15px">Delete job-purge-policy by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/job-purge-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceCreateDeletedResource(body, callback)</td>
    <td style="padding:15px">Create deleted-resource</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/deleted-resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceListDeletedResource(specSize, specPageMarker, specDetail, specCount, specExcludeShared, specExcludeHrefs, specParentFqNameStr, specParentType, specParentId, specBackRefId, specObjUuids, specFields, specFilters, specRefUuids, specFrom, specSortby, specOperation = 'AND', specTagFilters, specTagDetail, specRefFields, specExtRefUuids, callback)</td>
    <td style="padding:15px">List deleted-resources</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/deleted-resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkListDeletedResource(body, callback)</td>
    <td style="padding:15px">Bulk list deleted-resources</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-list-deleted-resource?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceUpdateDeletedResource(iD, body, callback)</td>
    <td style="padding:15px">Update deleted-resource by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/deleted-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceGetDeletedResource(iD, detail, fields, refFields, callback)</td>
    <td style="padding:15px">Get deleted-resource by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/deleted-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceDeleteDeletedResource(iD, callback)</td>
    <td style="padding:15px">Delete deleted-resource by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/deleted-resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceBulkListLastPublishedNotification(body, callback)</td>
    <td style="padding:15px">Bulk list last-published-notifications</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/bulk-list-last-published-notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceUpdateTask(iD, body, callback)</td>
    <td style="padding:15px">Update task by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceGetTask(iD, detail, fields, refFields, callback)</td>
    <td style="padding:15px">Get task by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceDeleteTask(iD, callback)</td>
    <td style="padding:15px">Delete task by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreServiceSync(body, callback)</td>
    <td style="padding:15px">Sync</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jobstoreManagerPurge(body, callback)</td>
    <td style="padding:15px">Purge</td>
    <td style="padding:15px">{base_path}/{version}/jobstore/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
