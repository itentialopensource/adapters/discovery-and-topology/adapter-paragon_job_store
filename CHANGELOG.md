
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_18:39PM

See merge request itentialopensource/adapters/adapter-paragon_job_store!11

---

## 0.3.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_job_store!9

---

## 0.3.2 [08-15-2024]

* Changes made at 2024.08.14_17:19PM

See merge request itentialopensource/adapters/adapter-paragon_job_store!8

---

## 0.3.1 [08-06-2024]

* Changes made at 2024.08.06_18:19PM

See merge request itentialopensource/adapters/adapter-paragon_job_store!7

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!6

---

## 0.2.4 [03-25-2024]

* Changes made at 2024.03.25_11:02AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!5

---

## 0.2.3 [03-13-2024]

* Changes made at 2024.03.13_12:46PM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!4

---

## 0.2.2 [03-11-2024]

* Update metadata

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!3

---

## 0.2.1 [02-26-2024]

* Changes made at 2024.02.26_10:57AM

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!2

---

## 0.2.0 [12-29-2023]

* More migration changes

See merge request itentialopensource/adapters/discovery-and-topology/adapter-paragon_job_store!1

---

## 0.1.1 [10-03-2023]

* Bug fixes and performance improvements

See commit e608e3e

---
