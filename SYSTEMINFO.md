# Paragon Job Store

Vendor: Juniper
Homepage: https://www.juniper.net

Product: Job Store (Paragon Automation)
Product Page: https://www.juniper.net/documentation/product/us/en/paragon-automation/

## Introduction
We classify Paragon Job Store into the Discovery & Topology domain as Paragon Job Store provides capabilities to enable automation of jobs related to the discovery and management of network devices and their configuration.

## Why Integrate
The Paragon Job Store adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon Job Store. With this adapter you have the ability to perform operations on items such as:

- Job Store Service

## Additional Product Documentation