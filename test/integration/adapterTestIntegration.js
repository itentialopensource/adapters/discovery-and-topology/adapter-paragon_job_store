/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_job_store',
      type: 'ParagonJobStore',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const ParagonJobStore = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Paragon_job_store Adapter Test', () => {
  describe('ParagonJobStore Class Tests', () => {
    const a = new ParagonJobStore(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const jobstoreServiceJobstoreServiceBulkExtRefUpdateBodyParam = {
      items: [
        {
          'ref-from-type': 'string',
          'ref-from-uuid': 'string',
          operation: 'string',
          'ref-to-uuid': 'string',
          'ref-to-type': 'string'
        }
      ]
    };
    describe('#jobstoreServiceBulkExtRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkExtRefUpdate(jobstoreServiceJobstoreServiceBulkExtRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkExtRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkListDeletedResourceBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: false,
        exclude_hrefs: true,
        tag_detail: false,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceBulkListDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkListDeletedResource(jobstoreServiceJobstoreServiceBulkListDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkListDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkListJobBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: false,
        exclude_hrefs: false,
        tag_detail: true,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceBulkListJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkListJob(jobstoreServiceJobstoreServiceBulkListJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkListJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkListJobPurgePolicyBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: true,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: true,
        exclude_hrefs: true,
        tag_detail: true,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceBulkListJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkListJobPurgePolicy(jobstoreServiceJobstoreServiceBulkListJobPurgePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkListJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkListLastPublishedNotificationBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: false,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: false,
        exclude_hrefs: true,
        tag_detail: false,
        count: false,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceBulkListLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkListLastPublishedNotification(jobstoreServiceJobstoreServiceBulkListLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkListLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkListTaskBodyParam = {
      spec: {
        ref_fields: [
          'string'
        ],
        parent_type: 'string',
        filters: [
          'string'
        ],
        operation: 'AND',
        json_filters: [
          {
            values: [
              'string'
            ],
            key: 'string'
          }
        ],
        size: 'string',
        from: 'string',
        page_marker: 'string',
        obj_uuids: [
          'string'
        ],
        ref_uuids_map: {},
        detail: false,
        parent_id: [
          'string'
        ],
        ext_ref_uuids: [
          'string'
        ],
        back_ref_id: [
          'string'
        ],
        exclude_shared: false,
        exclude_hrefs: false,
        tag_detail: true,
        count: true,
        fields: [
          'string'
        ],
        parent_fq_name_str: [
          'string'
        ],
        ref_uuids: [
          'string'
        ],
        sortby: 'string',
        tag_filters: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceBulkListTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkListTask(jobstoreServiceJobstoreServiceBulkListTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkListTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceBulkRefUpdateBodyParam = {
      refs: [
        {
          'ref-type': 'string',
          'ref-uuid': 'string',
          operation: 'string',
          type: 'string',
          uuid: 'string'
        }
      ],
      type: 'string',
      uuid: 'string'
    };
    describe('#jobstoreServiceBulkRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceBulkRefUpdate(jobstoreServiceJobstoreServiceBulkRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceBulkRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceCreateDeletedResourceBodyParam = {
      'deleted-resource': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        parent_type: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        href: 'string',
        meta: {
          enable: true,
          user_visible: false
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        data: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resource_type: 'string',
        uuid: 'string'
      }
    };
    describe('#jobstoreServiceCreateDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceCreateDeletedResource(jobstoreServiceJobstoreServiceCreateDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceCreateDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceExtRefUpdateBodyParam = {
      'ref-from-type': 'string',
      'ref-from-uuid': 'string',
      operation: 'string',
      'ref-to-uuid': 'string',
      'ref-to-type': 'string'
    };
    describe('#jobstoreServiceExtRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceExtRefUpdate(jobstoreServiceJobstoreServiceExtRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceExtRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceCreateJobBodyParam = {
      job: {
        parent_uuid: 'string',
        parent_type: 'string',
        callback_params: 'string',
        task_child_refs: [
          {
            uuid: 'string'
          }
        ],
        href: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        display_name: 'string',
        uuid: 'string',
        configuration_version: 'string',
        created_by: 'string',
        trigger: 'string',
        progress: 'string',
        type: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resources: [
          'string'
        ],
        metadata: [
          {
            value: 'string',
            key: 'string'
          }
        ],
        status: 'string',
        description: 'string',
        start_time: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        finish_time: 'string',
        status_message: 'string',
        schedule_time: 'string',
        meta: {
          enable: true,
          user_visible: false
        },
        callback_url: 'string'
      }
    };
    describe('#jobstoreServiceCreateJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceCreateJob(jobstoreServiceJobstoreServiceCreateJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceCreateJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceCreateJobPurgePolicyBodyParam = {
      'job-purge-policy': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        tenant_id: 'string',
        parent_type: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        retention_period: 'string',
        href: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        uuid: 'string'
      }
    };
    describe('#jobstoreServiceCreateJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceCreateJobPurgePolicy(jobstoreServiceJobstoreServiceCreateJobPurgePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceCreateJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceCreateLastPublishedNotificationBodyParam = {
      'last-published-notification': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        parent_type: 'string',
        last_published_timestamp: 'string',
        name: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        href: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resource_type: 'string',
        uuid: 'string'
      }
    };
    describe('#jobstoreServiceCreateLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceCreateLastPublishedNotification(jobstoreServiceJobstoreServiceCreateLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceCreateLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceRefUpdateBodyParam = {
      'ref-type': 'string',
      'ref-uuid': 'string',
      operation: 'string',
      type: 'string',
      uuid: 'string'
    };
    describe('#jobstoreServiceRefUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceRefUpdate(jobstoreServiceJobstoreServiceRefUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceRefUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceSyncBodyParam = {
      task: {
        resources: [
          {
            parent_uuid: 'string',
            parent_type: 'string',
            href: 'string',
            parent_task: 'string',
            created_timestamp: 'string',
            updated_by: 'string',
            updated_timestamp: 'string',
            task_type: 'string',
            display_name: 'string',
            uuid: 'string',
            configuration_version: 'string',
            created_by: 'string',
            progress: 'string',
            status_message: 'string',
            annotations: {
              key_value_pair: [
                {
                  value: 'string',
                  key: 'string'
                }
              ]
            },
            resources: [
              'string'
            ],
            status: 'string',
            description: 'string',
            start_time: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            finish_time: 'string',
            meta: {
              enable: false,
              user_visible: false
            }
          }
        ]
      },
      'last-published-notification': {
        resources: [
          {
            updated_timestamp: 'string',
            parent_uuid: 'string',
            description: 'string',
            configuration_version: 'string',
            parent_type: 'string',
            last_published_timestamp: 'string',
            name: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            href: 'string',
            meta: {
              enable: false,
              user_visible: true
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            annotations: {
              key_value_pair: [
                {
                  value: 'string',
                  key: 'string'
                }
              ]
            },
            resource_type: 'string',
            uuid: 'string'
          }
        ]
      },
      ignore_optimistic_lock: true,
      job: {
        resources: [
          {
            parent_uuid: 'string',
            parent_type: 'string',
            callback_params: 'string',
            task_child_refs: [
              {
                uuid: 'string'
              }
            ],
            href: 'string',
            created_timestamp: 'string',
            updated_by: 'string',
            updated_timestamp: 'string',
            display_name: 'string',
            uuid: 'string',
            configuration_version: 'string',
            created_by: 'string',
            trigger: 'string',
            progress: 'string',
            type: 'string',
            annotations: {
              key_value_pair: [
                {
                  value: 'string',
                  key: 'string'
                }
              ]
            },
            resources: [
              'string'
            ],
            metadata: [
              {
                value: 'string',
                key: 'string'
              }
            ],
            status: 'string',
            description: 'string',
            start_time: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            finish_time: 'string',
            status_message: 'string',
            schedule_time: 'string',
            meta: {
              enable: true,
              user_visible: true
            },
            callback_url: 'string'
          }
        ]
      },
      operation: 'string',
      'deleted-resource': {
        resources: [
          {
            updated_timestamp: 'string',
            parent_uuid: 'string',
            description: 'string',
            configuration_version: 'string',
            parent_type: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            href: 'string',
            meta: {
              enable: true,
              user_visible: true
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            data: 'string',
            annotations: {
              key_value_pair: [
                {
                  value: 'string',
                  key: 'string'
                }
              ]
            },
            resource_type: 'string',
            uuid: 'string'
          }
        ]
      },
      field_mask: {
        paths: [
          'string'
        ]
      },
      'job-purge-policy': {
        resources: [
          {
            updated_timestamp: 'string',
            parent_uuid: 'string',
            description: 'string',
            configuration_version: 'string',
            tenant_id: 'string',
            parent_type: 'string',
            perms2: {
              owner: 'string',
              global_access: 'string',
              share: [
                {
                  access: 'string',
                  scope: 'string',
                  levels: 'string',
                  scope_types: [
                    'string'
                  ]
                }
              ]
            },
            name: 'string',
            retention_period: 'string',
            href: 'string',
            meta: {
              enable: true,
              user_visible: false
            },
            created_by: 'string',
            updated_by: 'string',
            created_timestamp: 'string',
            annotations: {
              key_value_pair: [
                {
                  value: 'string',
                  key: 'string'
                }
              ]
            },
            uuid: 'string'
          }
        ]
      }
    };
    describe('#jobstoreServiceSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceSync(jobstoreServiceJobstoreServiceSyncBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceCreateTaskBodyParam = {
      task: {
        parent_uuid: 'string',
        parent_type: 'string',
        href: 'string',
        parent_task: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        task_type: 'string',
        display_name: 'string',
        uuid: 'string',
        configuration_version: 'string',
        created_by: 'string',
        progress: 'string',
        status_message: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resources: [
          'string'
        ],
        status: 'string',
        description: 'string',
        start_time: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        finish_time: 'string',
        meta: {
          enable: true,
          user_visible: true
        }
      }
    };
    describe('#jobstoreServiceCreateTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceCreateTask(jobstoreServiceJobstoreServiceCreateTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceCreateTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceListDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceListDeletedResource(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceListDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceID = 'fakedata';
    const jobstoreServiceJobstoreServiceUpdateDeletedResourceBodyParam = {
      ignore_optimistic_lock: false,
      'deleted-resource': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        parent_type: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        href: 'string',
        meta: {
          enable: true,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        data: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resource_type: 'string',
        uuid: 'string'
      },
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceUpdateDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceUpdateDeletedResource(jobstoreServiceID, jobstoreServiceJobstoreServiceUpdateDeletedResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceUpdateDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceGetDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceGetDeletedResource(jobstoreServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceGetDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceListJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceListJob(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceListJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceListJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceListJobPurgePolicy(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceListJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceUpdateJobPurgePolicyBodyParam = {
      ignore_optimistic_lock: false,
      ID: 'string',
      'job-purge-policy': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        tenant_id: 'string',
        parent_type: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        retention_period: 'string',
        href: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        uuid: 'string'
      },
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceUpdateJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceUpdateJobPurgePolicy(jobstoreServiceID, jobstoreServiceJobstoreServiceUpdateJobPurgePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceUpdateJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceGetJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceGetJobPurgePolicy(jobstoreServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceGetJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceUpdateJobBodyParam = {
      job: {
        parent_uuid: 'string',
        parent_type: 'string',
        callback_params: 'string',
        task_child_refs: [
          {
            uuid: 'string'
          }
        ],
        href: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        display_name: 'string',
        uuid: 'string',
        configuration_version: 'string',
        created_by: 'string',
        trigger: 'string',
        progress: 'string',
        type: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resources: [
          'string'
        ],
        metadata: [
          {
            value: 'string',
            key: 'string'
          }
        ],
        status: 'string',
        description: 'string',
        start_time: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        finish_time: 'string',
        status_message: 'string',
        schedule_time: 'string',
        meta: {
          enable: false,
          user_visible: true
        },
        callback_url: 'string'
      },
      ID: 'string',
      ignore_optimistic_lock: true,
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceUpdateJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceUpdateJob(jobstoreServiceID, jobstoreServiceJobstoreServiceUpdateJobBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceUpdateJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceGetJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceGetJob(jobstoreServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceGetJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceListLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceListLastPublishedNotification(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceListLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceUpdateLastPublishedNotificationBodyParam = {
      ignore_optimistic_lock: false,
      'last-published-notification': {
        updated_timestamp: 'string',
        parent_uuid: 'string',
        description: 'string',
        configuration_version: 'string',
        parent_type: 'string',
        last_published_timestamp: 'string',
        name: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        href: 'string',
        meta: {
          enable: true,
          user_visible: false
        },
        created_by: 'string',
        updated_by: 'string',
        created_timestamp: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resource_type: 'string',
        uuid: 'string'
      },
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceUpdateLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceUpdateLastPublishedNotification(jobstoreServiceID, jobstoreServiceJobstoreServiceUpdateLastPublishedNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceUpdateLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceGetLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceGetLastPublishedNotification(jobstoreServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceGetLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceListTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceListTask(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceListTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreServiceJobstoreServiceUpdateTaskBodyParam = {
      ignore_optimistic_lock: false,
      task: {
        parent_uuid: 'string',
        parent_type: 'string',
        href: 'string',
        parent_task: 'string',
        created_timestamp: 'string',
        updated_by: 'string',
        updated_timestamp: 'string',
        task_type: 'string',
        display_name: 'string',
        uuid: 'string',
        configuration_version: 'string',
        created_by: 'string',
        progress: 'string',
        status_message: 'string',
        annotations: {
          key_value_pair: [
            {
              value: 'string',
              key: 'string'
            }
          ]
        },
        resources: [
          'string'
        ],
        status: 'string',
        description: 'string',
        start_time: 'string',
        perms2: {
          owner: 'string',
          global_access: 'string',
          share: [
            {
              access: 'string',
              scope: 'string',
              levels: 'string',
              scope_types: [
                'string'
              ]
            }
          ]
        },
        name: 'string',
        finish_time: 'string',
        meta: {
          enable: false,
          user_visible: false
        }
      },
      ID: 'string',
      fieldMask: {
        paths: [
          'string'
        ]
      }
    };
    describe('#jobstoreServiceUpdateTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceUpdateTask(jobstoreServiceID, jobstoreServiceJobstoreServiceUpdateTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceUpdateTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceGetTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceGetTask(jobstoreServiceID, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceGetTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobstoreManagerJobstoreManagerPurgeBodyParam = {
      tenant_id: 'string'
    };
    describe('#jobstoreManagerPurge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreManagerPurge(jobstoreManagerJobstoreManagerPurgeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreManager', 'jobstoreManagerPurge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceDeleteDeletedResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceDeleteDeletedResource(jobstoreServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceDeleteDeletedResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceDeleteJobPurgePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceDeleteJobPurgePolicy(jobstoreServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceDeleteJobPurgePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceDeleteJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceDeleteJob(jobstoreServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceDeleteJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceDeleteLastPublishedNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceDeleteLastPublishedNotification(jobstoreServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceDeleteLastPublishedNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobstoreServiceDeleteTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobstoreServiceDeleteTask(jobstoreServiceID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_job_store-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobstoreService', 'jobstoreServiceDeleteTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
